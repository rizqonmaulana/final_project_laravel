<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

// Route::get('/', function () {
//     return view('pages.index');
// });
Route::get('/', function () {
    return view('adminlte.master');
});

Route::resource('pertanyaan', 'PertanyaanController');
Route::get('/{pertanyaan_id}/KomentarPertanyaan/create', 'KomentarPertanyaanController@create')->name('KomentarPertanyaan.create');
Route::post('/KomentarPertanyaan', 'KomentarPertanyaanController@store')->name('KomentarPertanyaan.store');
Route::get('/KomentarPertanyaan', 'KomentarJawabanController@index')->name('komentar.index');
Route::GET('/KomentarPertanyaan/{pertanyaan_id}/edit', 'KomentarPertanyaanController@edit')->name('KomentarPertanyaan.edit');
Route::put('/KomentarPertanyaan/{pertanyaan_id}', 'KomentarPertanyaanController@update')->name('KomentarPertanyaan.update');
Route::delete('/KomentarPertanyaan/{pertanyaan_id}', 'KomentarPertanyaanController@destroy')->name('KomentarPertanyaan.destroy');


Route::get('/like/pertanyaan/{id}', 'VotePertanyaanController@like')->name('like.jawaban');
Route::get('/dislike/pertanyaan/{id}', 'VotePertanyaanController@dislike')->name('dislike.jawaban');

Route::resource('profil', 'ProfilController');
Route::resource('jawaban', 'JawabanController');
Route::GET('/jawaban-terbaik/{pertanyaan_id}/{jawaban_id}', 'JawabanController@terbaik')->name('jawaban.terbaik');
Route::GET('/create-jawaban/{id}', 'JawabanController@create_jawaban')->name('create.jawaban');
Route::GET('/edit-jawaban/{id}/edit/{pertanyaan_id}', 'JawabanController@edit_jawaban')->name('edit.jawaban');
Route::DELETE('/destroy-jawaban/{id}/{pertanyaan_id}', 'JawabanController@destroy_jawaban')->name('destroy.jawaban');
Auth::routes();

// laravel file manager
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::get('/{jawaban_id}/komentar/create', 'KomentarJawabanController@create')->name('komentar.create');
Route::post('/komentar', 'KomentarJawabanController@store')->name('komentar.store');
Route::get('/{jawaban_id}/komentar', 'KomentarJawabanController@index')->name('komentar.index');
Route::get('/{id}/komentar/edit', 'KomentarJawabanController@edit')->name('komentar.edit');
Route::put('/{id}/komentar', 'KomentarJawabanController@update')->name('komentar.update');
Route::delete('/{id}/komentar', 'KomentarJawabanController@destroy')->name('komentar.destroy');

Route::get('/like/jawaban/{id}', 'VoteJawabanController@like')->name('like.jawaban');
Route::get('/dislike/jawaban/{id}', 'VoteJawabanController@dislike')->name('dislike.jawaban');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/user/list', 'HomeController@userList')->name('user.list');
Route::get('/home/user/pertanyaan', 'HomeController@userPertanyaan')->name('user.pertanyaan');
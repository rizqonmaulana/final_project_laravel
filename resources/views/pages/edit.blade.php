@extends('adminlte.master');

{{-- laravel file manager --}}
@include('scripts.lfm-head');

@section('content')
<form role="form" action=" {{ route('pertanyaan.update', ['pertanyaan' => $post->id]) }} " method="POST">
  @csrf
  @method('PUT')
    <div class="card-body">
      <h3>Edit question {{ $post->id }} </h3>
      <div class="form-group">
        <label for="judul">judul</label>
        <input type="text" class="form-control" id="judul" name="judul" value="{{ old('title', $post->judul) }}" placeholder="Masukan judul">
      </div>
      <div class="form-group">
        <label for="isi">isi</label>
        <textarea name="isi" class="form-control my-editor" placeholder="Insert isi">{!! old('isi', $post->isi) !!}</textarea>
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Edit</button>
    </div>
  </form>
@endsection

{{-- laravel file manager --}}
@include('scripts.lfm-bot');

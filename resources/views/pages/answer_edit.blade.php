@extends('adminlte.master');

{{-- laravel file manager --}}
@include('scripts.lfm-head')

@section('content')
<form role="form" action=" {{ route('jawaban.update', ['jawaban' => $jawaban->id]) }} " method="POST">
  @csrf
  @method('PUT')
    <div class="card-body">
      <h3>Edit Jawaban</h3>
      <div class="form-group">
        <label for="isi">Isi</label>
        <input type="hidden" name="pertanyaan_id" value=" {{ $pertanyaan->id }} ">
        <textarea name="isi" class="form-control my-editor" placeholder="Masukan isi">{!! old('isi', $jawaban->isi ) !!}</textarea>
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Edit</button>
    </div>
  </form>
@endsection

{{-- laravel file manager --}}
@include('scripts.lfm-bot')

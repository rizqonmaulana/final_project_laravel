@extends('adminlte.master');

{{-- laravel file manager --}}
@include('scripts.lfm-head')

@section('content')
<form role="form" action=" {{ route('pertanyaan.store') }} " method="POST">
  @csrf
    <div class="card-body">
      <h3>Post a question</h3>
      <div class="form-group">
        <label for="judul">Judul</label>
        <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Masukan judul">
      </div>
      <div class="form-group">
        <label for="isi">Isi</label>
        <textarea name="isi" class="form-control my-editor">{!! old('isi', '') !!}</textarea>
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection

{{-- laravel file manager --}}
@include('scripts.lfm-bot')

@push('scripts')
  <script>
      Swal.fire({
          title: 'Berhasil!',
          text: 'Memasangkan script sweet alert',
          icon: 'success',
          confirmButtonText: 'Cool'
      })
  </script>
@endpush
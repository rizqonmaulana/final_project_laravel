@extends('adminlte.master')

@section('content')
<form role="form" action=" {{ route('KomentarPertanyaan.update', ['pertanyaan_id' => $post->id]) }} " method="POST">
  @csrf
  @method('PUT')
    <div class="card-body">
      <h3>Ubah Komentar Pertanyaan</h3>
      <div class="form-group">
        <label for="isi">Isi</label>
        
        <textarea type="text" class="form-control" id="isi" name="isi" placeholder="Masukan isi">{{ old('isi', $post->isi) }}</textarea>

      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection

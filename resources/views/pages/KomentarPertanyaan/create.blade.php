@extends('adminlte.master')

@section('content')
<form role="form" action=" {{ route('KomentarPertanyaan.store') }} " method="POST">
  @csrf
    <div class="card-body">
      <h3>Beri Komentar Pertanyaan</h3>
      <div class="form-group">
        <label for="isi">Isi</label>
        <input type="hidden" name="pertanyaan_id" value="{{ $pertanyaan_id}}">
        <textarea type="text" class="form-control" id="isi" name="isi" placeholder="Masukan isi">{{ old('isi', '') }}</textarea>

      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection

@push('script')
  @if (session('success'))
    <script>
        Swal.fire({
            title: 'Berhasil!',
            text: 'Memasangkan script sweet alert',
            icon: 'success',
            confirmButtonText: 'Cool'
        })
    </script>
  @endif
@endpush

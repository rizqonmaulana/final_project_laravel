@extends('adminlte.master')

@section('content')
    <div class="card m-3">
        <div class="card-header">
            <h1>Daftar Komentar Untuk:</h1>
            <h2>{{ $pertanyaan->isi }}</h2>
        </div>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                @forelse($komentar as $komentar)
                <li class="list-group-item">
                    <p>{{ $komentar->isi }}</p>
                    <p style="font-size: 12px">Pada : {{ $komentar->updated_at }} </p>
                </li>
                @empty
                <li class="list-group-item">Tidak ada komentar</li>
                @endforelse
            </ul>
        </div>
        <div class="card-footer">
            <a href="{{ route('pertanyaan.show', ['pertanyaan' => $pertanyaan->pertanyaan_id]) }}">Kembali</a>
        </div>
    </div>
@endsection

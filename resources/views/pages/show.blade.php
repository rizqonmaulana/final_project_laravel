@extends('adminlte.master')

@php
    use App\VoteJawaban;
@endphp

@php
    use App\VotePertanyaan;
@endphp

@section('content')
    <div class="mt-3 mx-3">
    @if (session('success'))
        <div class="alert alert-success m-1">
            {{ session('success') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
          <h1 class="card-title" style="font-size: 20px; font-weight:bold;">{{ $post->judul }}</h1>
        </div>
        <div class="card-body">
            {!! $post->isi !!}
        </div>
        <hr>
        <div class="mx-3">
            <p style="font-size: 12px">ditanyakan oleh : {{ isset($post->user->profil->nama) ? $post->user->profil->nama : 'anonim' }} </p>
            <p style="font-size: 12px">ditanyakan pada : {{ $post->created_at }} </p>
        </div>
        <tr>
            {{-- tombol jawab dan komentar --}}
    <table class="mx-3 mb-2">
        <tbody>
            <td  style="display: flex;">

                @if (Auth::user()->id != $post->user_id)
                    <a href="{{ route('create.jawaban',$post->id ) }}" class="btn btn-info btn-sm mr-1" style="width: 120px; font-size:12px;">Jawab Pertanyaan</a>
                    <a href="{{ route('KomentarPertanyaan.create',$post->id) }}" class="btn btn-warning btn-sm mr-1" style="width: 120px; font-size:12px;">Beri Komentar</a>
                @endif

                @if (Auth::user()->id == $post->user_id)
                    <a href="{{ route('pertanyaan.edit', ['pertanyaan' => $post->id]) }}" class="btn btn-info btn-sm mr-1">edit</a>
                    <form action=" {{ route('pertanyaan.destroy', ['pertanyaan' => $post->id]) }} " method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                @endif
            </td>
        </tbody>
    </table>
        </tr>
      </div>
    </div>

    @foreach($post->Komentar as $jawaban)
    <div class="card mx-3">
        <div class="card-body">
            {{$jawaban->isi}}
        </div>
        <hr>
        <div class="mx-3 row">
            <div class="col-6">
                <p style="font-size: 12px">diberi komentar pada : {{ $jawaban->created_at }} </p>
            </div>
        </div>
        <table class="mx-3 mb-2">
            <tbody>
                <td style="display: flex;">
                    <a href="{{ route('KomentarPertanyaan.edit', ['pertanyaan_id' => $jawaban->id]) }}" class="btn btn-info btn-sm mr-1"  style="width: 60px; font-size:12px;">edit</a>
                    <form action=" {{ route('KomentarPertanyaan.destroy', ['pertanyaan_id' => $jawaban->id]) }} " method="POST" class="mr-2">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tbody>
        </table>
    </div>
    <br>
    @endforeach

    @foreach($post->jawaban as $jawaban)
    <div class="card mx-3">
        <div class="card-body">

            @if ($post->jawaban_tepat_id == $jawaban->id)
                <p style="background-color: green; font-size: 12px; color: white; width: 205px; border-radius: 5px; padding:5px;">Jawaban terbaik, dipilih oleh penanya</p>
            @endif


            {!! $jawaban->isi !!}
        </div>
        <hr>
        <div class="ml-3">
            <p style="font-size: 12px">dijawab oleh : {{ isset($jawaban->user->profil->nama) ? $jawaban->user->profil->nama : 'anonim' }} </p>
            <p style="font-size: 12px">dijawab pada : {{ $jawaban->created_at }} </p>
        <div class="mx-3 row">
            <div class="col-6">

            </div>
            <div class="col-6 text-right">
                <a href="{{ route('like.jawaban', ['id' => $jawaban->id]) }}">Like</a>
                @php
                    $votes = VoteJawaban::where('jawaban_id', $jawaban->id)->get();
                    $poin_arr = [];
                    foreach($votes as $vote) {
                        $poin_arr[] = $vote->poin;
                    }
                    $total_poin = array_sum($poin_arr);
                @endphp
                <span class="mx-1 text-bold">| Poin: {{ $total_poin }} |</span>
                <a href="{{ route('dislike.jawaban', ['id' => $jawaban->id]) }}">Dislike</a>
            </div>
        </div>
    <table class="mx-3 mb-2">
        <tbody>
            <td style="display: flex;">
                @if (Auth::user()->id == $post->user_id)
                <a href="{{ route('jawaban.terbaik', ['pertanyaan_id' => $post->id ,'jawaban_id' => $jawaban->id]) }}" class="btn btn-success btn-sm mx-1" style="width: 120px; font-size:12px;">Jawaban Terbaik</a>
                @endif

                @if (Auth::user()->id != $jawaban->user_id)
                    <a href="{{ route('komentar.create', ['jawaban_id' => $jawaban->id]) }}" class="btn btn-warning btn-sm mx-1" style="width: 120px; font-size:12px;">Beri Komentar</a>
                @endif

                @if (Auth::user()->id == $jawaban->user_id)
                    <a href="{{ route('edit.jawaban', ['id'=>$jawaban->id, 'pertanyaan_id'=>$post->id]) }}" class="btn btn-info btn-sm mr-1"  style="width: 60px; font-size:12px;">edit</a>
                    <form action=" {{ route('destroy.jawaban', ['id' => $jawaban->id, 'pertanyaan_id'=>$post->id]) }} " method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                @endif
                <a href="{{ route('komentar.index', ['jawaban_id' => $jawaban->id]) }}">Lihat Komentar</a>
            </td>
        </tbody>
    </table>
    </div>
</div>
    <br>
    @endforeach

@endsection

@push('script')
    <script>
        Swal.fire({
            title: 'Berhasil!',
            text: 'Memasangkan script sweet alert',
            icon: 'success',
            confirmButtonText: 'Cool'
        })
    </script>
@endpush
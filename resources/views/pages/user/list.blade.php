@extends('adminlte.master')

@section('content')
<div class="mt-3 mx-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar Pengguna</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($users_list as $key => $user)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    @if(isset($user->profil))
                        <td>{{ $user->profil->nama }}</td>
                    @else
                        <td>Anonim</td>
                    @endif
                    <td>{{ $user->email }}</td>
                </tr>
                @empty
                  <tr>
                      <td colspan="3" align="center"> No Data </td>
                  </tr>
                @endforelse
            </tbody>
          </table>
        </div>
      </div>
</div>
@endsection

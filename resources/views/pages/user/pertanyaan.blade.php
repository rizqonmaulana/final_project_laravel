@extends('adminlte.master')

@section('content')
<div class="mt-3 mx-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar Pertanyaan Anda</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Pertanyaan</th>
                <th style="width: 40px">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($userPertanyaan as $key => $pertanyaan)
                <tr>
                    <td> {{ $key + 1 }}</td>
                    <td> {{ $pertanyaan->judul }} </td>
                    <td  style="display: flex;">
                        <a href="{{ route('pertanyaan.show', ['pertanyaan' => $pertanyaan->id]) }}" class="btn btn-info btn-sm mr-1">show</a>

                        @if (Auth::user()->id == $pertanyaan->user_id)
                            <a href="{{ route('pertanyaan.edit', ['pertanyaan' => $pertanyaan->id]) }}" class="btn btn-warning btn-sm mr-1">edit</a>
                            <form action=" {{ route('pertanyaan.destroy', ['pertanyaan' => $pertanyaan->id]) }} " method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger btn-sm" value="delete">
                            </form>
                        @endif

                    </td>
                  </tr>
                @empty
                  <tr>
                      <td colspan="3" align="center">Anda belum ada membuat pertanyaan</td>
                  </tr>
                @endforelse
            </tbody>
          </table>
        </div>
      </div>
</div>
@endsection

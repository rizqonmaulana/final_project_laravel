@extends('adminlte.master')

{{-- laravel file manager --}}
@include('scripts.lfm-head')

@section('content')
<form role="form" action=" {{ route('komentar.store') }} " method="POST">
  @csrf
    <div class="card-body">
      <h3>Beri Komentar</h3>
      <div class="form-group">
        <label for="isi">Isi</label>
        <input type="hidden" name="jawaban_id" value="{{ $jawaban_id }}">
        <textarea name="isi" class="form-control my-editor" placeholder="Masukan isi">{!! old('isi', '') !!}</textarea>
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection

@include('scripts.lfm-bot')

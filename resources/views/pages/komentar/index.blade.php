@extends('adminlte.master')

@section('content')
    <div class="card m-3">
    <div class="card-header">
        <h1>Daftar Komentar Untuk:</h1>
        <h2>{!! $jawaban->isi !!}</h2>
    </div>
    <div class="card-body">
        <ul class="list-group list-group-flush">
            @forelse($jawaban->komentarJawaban as $komentar)
            <li class="list-group-item">
                <p>{!! $komentar->isi !!}</p>
                <p style="font-size: 12px">Pada : {{ $komentar->updated_at }} </p>
                <a href="{{ route('komentar.edit', ['id' => $komentar->id]) }}">Ubah </a>
                <span> | </span>
                <form action=" {{ route('komentar.destroy', ['id' => $komentar->id]) }} " method="POST" style="display: inline;">
                    @csrf
                    @method('DELETE')
                    <input type="submit" style="background: none; border: none; color: #007BFF;" value="Hapus">
                </form>

            </li>
            @empty
            <li class="list-group-item">Tidak ada komentar</li>
            @endforelse
        </ul>
    </div>
    <div class="card-footer">
        <a href="{{ route('pertanyaan.show', ['pertanyaan' => $jawaban->pertanyaan_id]) }}">Kembali</a>
    </div>
    </div>
@endsection

@extends('adminlte.master')

{{-- laravel file manager --}}
@include('scripts.lfm-head')

@section('content')
<form role="form" action=" {{ route('komentar.update', ['id' => $komentar->id]) }} " method="POST">
  @csrf
  @method('PUT')
    <div class="card-body">
      <h3>Ubah Komentar</h3>
      <div class="form-group">
        <label for="isi">Isi</label>

        <textarea name="isi" class="form-control my-editor" placeholder="Masukan isi">{!! old('isi', $komentar->isi) !!}</textarea>

      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection

@include('scripts.lfm-bot')

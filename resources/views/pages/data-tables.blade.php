@extends('adminlte.master')

@section('content')
<h1 class="mt-4 mb-4">Profil DataTable</h2>
    <table id="data-tables" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>email</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts as $post)
            <tr>
                    <td>{{ $post->nama }}</td>
                    <td>{{ $post->user->email }}</td>
                </tr>
            @endforeach
    </table>

@endsection

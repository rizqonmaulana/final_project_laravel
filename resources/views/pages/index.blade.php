@extends('adminlte.master')

@section('content')
<div class="mt-3 mx-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
              <a href=" {{ route('pertanyaan.create') }} " class="btn btn-primary mb-2">Buat pertanyaan baru</a>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Pertanyaan</th>
                <th style="width: 40px">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($posts as $key => $post)
                <tr>
                    <td> {{ $key + 1 }}</td>
                    <td> {{ $post->judul }} </td>
                    <td  style="display: flex;">
                        <a href="{{ route('pertanyaan.show', ['pertanyaan' => $post->id]) }}" class="btn btn-info btn-sm mr-1">show</a>

                        @if (Auth::user()->id == $post->user_id)
                            <a href="{{ route('pertanyaan.edit', ['pertanyaan' => $post->id]) }}" class="btn btn-warning btn-sm mr-1">edit</a>
                            <form action=" {{ route('pertanyaan.destroy', ['pertanyaan' => $post->id]) }} " method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger btn-sm" value="delete">
                            </form>
                        @endif

                    </td>
                  </tr>
                @empty
                  <tr>
                      <td colspan="4" align="center"> No Data </td>
                  </tr>
                @endforelse
            </tbody>
          </table>
        </div>
      </div>
</div>
@endsection

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    //
    protected $guarded = [];
    protected $table = 'jawaban';

    public function pertanyaan()
    {
        return $this->belongsTo('App\Pertanyaan', 'pertanyaan_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function komentarJawaban()
    {
        return $this->hasMany('App\KomentarJawaban', 'jawaban_id');
    }
}

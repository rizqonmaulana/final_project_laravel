<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DislikePertanyaan extends Model
{
    protected $table = "dislikepertanyaan";

    protected $fillable =["point"];

    protected $guarded = [];

    public function pertanyaan(){
        return $this->belongsTo('App\Pertanyaan', 'id');
    }
}

<?php

namespace App;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    //
    protected $guarded = [];
    protected $table = 'pertanyaan';

    public function jawaban()
    {
        return $this->hasMany('App\Jawaban', 'pertanyaan_id');
    }

    public function komentar()
    {
        return $this->hasMany('App\KomentarPertanyaan', 'pertanyaan_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikePertanyaan extends Model
{
    protected $table = "likepertanyaan";

    protected $fillable =["point"];

    protected $guarded = [];

    public function pertanyaan(){
        return $this->belongsTo('App\Pertanyaan', 'id');
    }
}

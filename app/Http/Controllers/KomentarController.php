<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use App\KomentarPertanyaan;
use Auth;
use App\Pertanyaan;
use App\Pertanyaan as AppPertanyaan;
class KomentarPertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id){
        return view('pages.KomentarPertanyaan.create', ['pertanyaan_id' => $id]);
    }

    public function store(Request $request){

        $validatedData = $request->validate([
            'isi' => 'required',
            'pertanyaan_id' => 'required'
        ]);

        $post = Pertanyaan::find($request['pertanyaan_id']);

        $post->komentar()->create([
            'isi' => $request['isi'],
            'user_id' => Auth::user()->id
        ]);

        Alert::success('Berhasil', 'Berhasil menambahkan komentar');
    
        return redirect('/pertanyaan')->with(['success' => 'Komentar ditambah!']);
    }

    public function index($pertanyaan_id) {
        $posts = Pertanyaan::find($pertanyaan_id);

        return view('KomentarPertanyaan.index', ['pertanyaan' => $post]);
    }

    public function show($id) {
        $post = KomentarPertanyaan::find($id);

        return view('KomentarPertanyaan.show', ['id' => $post]);
    }

    public function edit($id) {
        $post = KomentarPertanyaan::find($id);

        return view('pages.KomentarPertanyaan.edit', compact('post'));
    }

    public function update($id, Request $request){

        $request->validate([
            'isi' => 'required'
        ]);

        $post = KomentarPertanyaan::where('id', $id)->update([
            'isi' => $request['isi']
        ]);

        Alert::success('Berhasil', 'Berhasil mengubah komentar');

        return redirect('/pertanyaan')->with('success');
    }

    public function destroy($id){
        KomentarPertanyaan::destroy($id);

        Alert::success('Berhasil', 'Berhasil menghapus komentar');

        return redirect('/pertanyaan')->with('success', 'Komentar berhasil dihapus');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Jawaban;

class VoteJawabanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function like($id)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $isUserVote = $user->jawaban()->where('jawaban_id', $id)->get();
        $pertanyaanId = Jawaban::find($id)->pertanyaan()->first()->id;

        if (count($isUserVote) === 0) {
            $user->jawaban()->attach($id, [
                'poin' => 10
            ]);
            return redirect("/pertanyaan/{$pertanyaanId}")->with(['success' => 'Jawaban divote!']);
        } else {
            // Update poin vote
            $user->jawaban()->updateExistingPivot($id, [
                'poin' => 10
            ]);
            return redirect("/pertanyaan/{$pertanyaanId}")->with(['success' => 'Vote diperbarui!']);
        }
    }

    public function dislike($id)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $isUserVote = $user->Jawaban()->where('jawaban_id', $id)->get();
        $pertanyaanId = Jawaban::find($id)->pertanyaan()->first()->id;

        if (count($isUserVote) === 0) {
            $user->jawaban()->attach($id, [
                'poin' => -2
            ]);
            return redirect("/pertanyaan/{$pertanyaanId}")->with(['success' => 'Jawaban divote!']);
        } else {
            // Update poin vote
            $user->jawaban()->updateExistingPivot($id, [
                'poin' => -2
            ]);
            return redirect("/pertanyaan/{$pertanyaanId}")->with(['success' => 'Vote diperbarui!']);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DislikeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('post.create');
    }

    public function store(Request $request, $pertanyaan_id){

        $pertanyaan = DislikePertanyaan::find($pertanyaan_id);

        $like = DislikePertanyaan::create([
            "point" => $request["point"],
            "user_id" => Auth::id(),
            "pertanyaan_id" => $request($pertanyaan)
        ]);

        return redirect('/pertanyaan')->with('success', 'berhasil like pertanyaan ini');
    }

    public function index() {
        $posts = DislikePertanyaan::all();

        return view('post.index', compact('posts'));
    }

    public function show($id) {
        $post = DisikePertanyaan::find($id);

        return view('post.show', compact('post'));
    }

    public function edit($id) {
        $post = DislikePertanyaan::find($id);

        return view('post.edit', compact('post'));
    }

    public function update($pertanyaan_id, $id, Request $request){

        $pertanyaan = DislikePertanyaan::find($pertanyaan_id);

        $update = DislikePertanyaan::where('id', $id)->update([
            "point" => $request["point"],
            "user_id" => Auth::id(),
            "pertanyaan_id" => $request($pertanyaan) 
        ]);

        return redirect('/pertanyaan')->with('success');
    }

    public function destroy($id){
        DislikePertanyaan::destroy($id);

        return redirect('/pertanyaan')->with('success');
    }
}

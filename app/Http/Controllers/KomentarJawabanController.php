<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\KomentarJawaban;
use Illuminate\Support\Facades\Auth;;
use App\Jawaban;

class KomentarJawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($jawaban_id)
    {
        $jawaban = Jawaban::find($jawaban_id);
        
        return view('pages.komentar.index', ['jawaban' => $jawaban]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('pages.komentar.create', ['jawaban_id' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'isi' => 'required|max:3000',
            'jawaban_id' => 'required'
        ]);
    
        $jawaban = Jawaban::find($request['jawaban_id']);

        $jawaban->komentarJawaban()->create([
            'isi' => $request['isi'],
            'user_id' => Auth::user()->id
        ]);

        Alert::success('Berhasil', 'Berhasil menambahkan komentar');
    
        return redirect('/pertanyaan')->with(['success' => 'Komentar ditambah!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $komentar = KomentarJawaban::find($id);
        return view('pages.komentar.edit', ['komentar' => $komentar]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'isi' => 'required|max:3000'
        ]);
    
        KomentarJawaban::find($id)->update([
            'isi' => $request['isi'],
        ]);

        Alert::success('Berhasil', 'Berhasil mengubah komentar');
    
        return redirect('/pertanyaan')->with(['success' => 'Komentar diubah!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        KomentarJawaban::destroy($id);

        Alert::success('Berhasil', 'Berhasil menghapus komentar');

        return redirect('/pertanyaan')->with(['success' => 'Komentar dihapus!']);
    }
}
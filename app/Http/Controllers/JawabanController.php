<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use App\Jawaban;
use Illuminate\Http\Request;
use App\Pertanyaan;
use Illuminate\Support\Facades\Auth;

class JawabanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_jawaban($id)
    {
        $data = Pertanyaan::findOrFail($id);
        return view('pages.answer', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required'
        ]);

        $post = Jawaban::create([

            "isi"           => $request["isi"],
            'pertanyaan_id' => $request->pertanyaan_id,
            "user_id"       => Auth::user()->id
        ]);

        Alert::success('Berhasil', 'Berhasil menambahkan jawaban');

        return redirect('pertanyaan/' . $request->pertanyaan_id)->with('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Jawaban::find($id);
        return view('pages.show', compact('post'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit_jawaban($id, $pertanyaan_id)
    {
        $jawaban = Jawaban::find($id);

        if ($jawaban->user_id != Auth::user()->id) {
            return redirect('/pertanyaan/' . $pertanyaan_id);
        } else {
            $pertanyaan = Pertanyaan::findOrFail($pertanyaan_id);
            return view('pages.answer_edit', compact('jawaban', 'pertanyaan'));
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'isi' => 'required'
        ]);

        $post = Jawaban::where('id', $id)->update([
            'isi' => $request['isi']
        ]);

        Alert::success('Berhasil', 'Berhasil mengubah jawaban');

        return redirect('/pertanyaan/' . $request->pertanyaan_id)->with('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_jawaban($id, $pertanyaan_id)
    {
        $jawaban = Jawaban::find($id);

        Alert::success('Berhasil', 'Berhasil menghapus jawaban');

        if ($jawaban->user_id != Auth::user()->id) {
            return redirect('/pertanyaan/' . $pertanyaan_id);
        } else {
            Jawaban::destroy($id);
            return redirect('/pertanyaan/' . $pertanyaan_id)->with('success');
        }
    }

    public function terbaik($pertanyaan_id, $jawaban_id)
    {
        $post = Pertanyaan::where('id', $pertanyaan_id)->update([
            'jawaban_tepat_id' => $jawaban_id
        ]);
        return redirect('/pertanyaan/' . $pertanyaan_id);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Pertanyaan;

class VotePertanyaanController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }

    public function like($id)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $isUserVote = $user->pertanyaan()->where('pertanyaan_id', $id)->get();
        $pertanyaanId = Pertanyaan::find($id)->first();

        if(count($isUserVote) === 0){
            $user->pertanyaan()->attach($id, [
                'poin' => 10
            ]);
            return redirect("/pertanyaan/{$pertanyaanId}")->with(['success' => 'Pertanyaan divote!']);
        } else{
            // Update poin vote
            $user->pertanyaan()->updateExistingPivot($id, [
                'poin' => 10
            ]);
            return redirect("/pertanyaan/{$pertanyaanId}")->with(['success' => 'Vote diperbarui!']);
        }
    }

    public function dislike($id)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $isUserVote = $user->pertanyaan()->where('pertanyaan_id', $id)->get();
        $pertanyaanId = Pertanyaan::find($id)->first();

        if(count($isUserVote) === 0){
            $user->pertanyaan()->attach($id, [
                'poin' => -2
            ]);
            return redirect("/pertanyaan/{$pertanyaanId}")->with(['success' => 'Pertanyaan divote!']);
        } else{
            // Update poin vote
            $user->pertanyaan()->updateExistingPivot($id, [
                'poin' => -2
            ]);
            return redirect("/pertanyaan/{$pertanyaanId}")->with(['success' => 'Vote diperbarui!']);
        }
    }
}

<?php

namespace App\Http\Controllers;
use DB;
use App\KomentarPertanyaan;
use Auth;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('post.create');
    }

    public function store(Request $request, $pertanyaan_id){

        $pertanyaan = LikePertanyaan::find($pertanyaan_id);

        $like = LikePertanyaan::create([
            "point" => $request["point"],
            "user_id" => Auth::id(),
            "pertanyaan_id" => $request($pertanyaan)
        ]);

        return redirect('/pertanyaan')->with('success', 'berhasil like pertanyaan ini');
    }

    public function index() {
        $posts = LikePertanyaan::all();

        return view('post.index', compact('posts'));
    }

    public function show($id) {
        $post = LikePertanyaan::find($id);

        return view('post.show', compact('post'));
    }

    public function edit($id) {
        $post = LikePertanyaan::find($id);

        return view('post.edit', compact('post'));
    }

    public function update($pertanyaan_id, $id, Request $request){

        $pertanyaan = LikePertanyaan::find($pertanyaan_id);

        $update = LikePertanyaan::where('id', $id)->update([
            "point" => $request["point"],
            "user_id" => Auth::id(),
            "pertanyaan_id" => $request($pertanyaan) 
        ]);

        return redirect('/pertanyaan')->with('success');
    }

    public function destroy($id){
        LikePertanyaan::destroy($id);

        return redirect('/pertanyaan')->with('success');
    }
}

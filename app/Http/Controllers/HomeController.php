<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect('/pertanyaan');
    }

    public function userList()
    {
        $users_list = User::all();
        return view('pages.user.list', ['users_list' => $users_list]);
    }

    public function userPertanyaan()
    {
        $userId = Auth::user()->id;
        $userPertanyaan = User::find($userId)->pertanyaanUser;
        return view('pages.user.pertanyaan', ['userPertanyaan' => $userPertanyaan]);
    }
}
